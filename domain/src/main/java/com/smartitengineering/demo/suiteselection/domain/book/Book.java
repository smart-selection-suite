/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.suiteselection.domain.book;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author imyousuf
 */
public class Book extends AbstractDomain {

    public static final String PROPERTYNAME_NAME = "name";
    public static final String PROPERTYNAME_AUTHORS = "authors";
    public static final String PROPERTYNAME_STOCKAMOUNT = "stockAmount";
    public static final String PROPERTYNAME_STRING_STOCKAMOUNT = "stringStockAmount";
    public static final String PROPERTYNAME_PRICE = "price";
    public static final String PROPERTYNAME_PUBLISHED = "published";
    public static final String PROPERTYNAME_EBOOKAVAILABLE = "ebookAvailable";
    private String name;
    private Set<Author> authors;
    private String stringStockAmount = "25";

    public String getStringStockAmount() {
        return stringStockAmount;
    }

    public void setStringStockAmount(String stringStockAmount) {

        //setInt(stringStockAmount);
        String oldValue = this.stringStockAmount;
        this.stringStockAmount = stringStockAmount;
        firePropertyChanged(PROPERTYNAME_STRING_STOCKAMOUNT, oldValue, stringStockAmount);

    }
    private int stockAmount;
    private double price;
    private boolean published;
    private boolean ebookAvailable;

    public Set<Author> getAuthors() {
        if (authors == null) {
            authors = new HashSet<Author>();
        }
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        Set<Author> oldAuthors = this.authors;
        this.authors = authors;
        this.<Set<Author>>firePropertyChanged(PROPERTYNAME_AUTHORS, oldAuthors, authors);
    }

    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    public void setName(String newName) {
        String oldName = this.name;
        this.name = newName;
        this.firePropertyChanged(PROPERTYNAME_NAME, oldName, newName);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStockAmount() {
        return stockAmount;
    }

    public void setStockAmount(int stockAmount) {


       // setString(stockAmount);
        int oldStockAmount = this.stockAmount;
        this.stockAmount = stockAmount;
        this.firePropertyChanged(PROPERTYNAME_NAME, oldStockAmount, stockAmount);

    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public boolean isEbookAvailable() {
        return ebookAvailable;
    }

    public void setEbookAvailable(boolean ebookAvailable) {
        this.ebookAvailable = ebookAvailable;
    }
    /*private void setInt(String stringStockAmount) {
    this.stockAmount = new Integer(stringStockAmount);
    }
    
    private void setString(int stockAmount) {
    this.stringStockAmount = Integer.toString(stockAmount);
    }*/
}

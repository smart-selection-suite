/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.suiteselection.domain.book;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author modhu7
 */
public class AbstractDomain {

    private Set<PropertyChangeListener> propertyChangeListeners;
    

    {
        propertyChangeListeners = new HashSet<PropertyChangeListener>();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listener != null) {
            propertyChangeListeners.add(listener);
        }
    }

    protected <T> void firePropertyChanged(String propertyName, T oldValue, T newValue) {
        PropertyChangeEvent changeEvent = new PropertyChangeEvent(this, propertyName, oldValue, newValue);
        for (PropertyChangeListener listener : propertyChangeListeners) {
            listener.propertyChange(changeEvent);
        }
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        if (listener != null) {
            propertyChangeListeners.remove(listener);
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.suiteselection.domain;

import com.smartitengineering.demo.suiteselection.domain.book.Author;
import com.smartitengineering.demo.suiteselection.domain.book.Book;
import com.smartitengineering.demo.suiteselection.domain.person.Person;
import com.smartitengineering.demo.suiteselection.domain.person.Pet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author imyousuf
 */
public class DomainFactory {

    private static List<Person> persons;
    private static List<Book> books;

    public static List<Person> getPersons() {
        if (persons == null) {
            persons = new ArrayList<Person>();
            Person person, child, grandChild;
            person = new Person();
            person.setName("Nurudding Miah");
            person.setDead(true);
            child = new Person();
            child.setName("A. H. M. Yousuf");
            grandChild = new Person();
            grandChild.setName("Imran M Yousuf");
            grandChild.getPets().add(getPet("Papila"));
            grandChild.getPets().add(getPet("Laden"));
            child.getChildren().add(grandChild);
            person.getChildren().add(child);
            child = new Person();
            child.setName("A. K. M. Shahbubul Alam");
            grandChild = new Person();
            grandChild.setName("Lenin");
            child.getChildren().add(grandChild);
            grandChild = new Person();
            grandChild.setName("Nayan");
            child.getChildren().add(grandChild);
            grandChild = new Person();
            grandChild.setName("Fariya");
            child.getChildren().add(grandChild);
            person.getChildren().add(child);
            child = new Person();
            child.setName("A. K. M. Mahbubul Alam");
            grandChild = new Person();
            grandChild.setName("Setu");
            child.getChildren().add(grandChild);
            grandChild = new Person();
            grandChild.setName("Emon");
            child.getChildren().add(grandChild);
            person.getChildren().add(child);
            persons.add(person);
            person = new Person();
            person.setName("Arun Bikas Sen Gupta");
            person.setDead(true);
            child = new Person();
            child.setName("Subrata Sen Gupta");
            child.getPets().add(getPet("Tomy"));
            child.getPets().add(getPet("Chintu"));
            person.getChildren().add(child);
            child = new Person();
            child.setName("Debobrata Sen Gupta");
            person.getChildren().add(child);
            persons.add(person);
        }
        return persons;
    }

    private static Pet getPet(String petName) {
        Pet pet = new Pet();
        pet.setName(petName);
        return pet;
    }

    public static List<Book> getBooks() {
        if (books == null) {
            books = new ArrayList<Book>();
            Author firstAuthor = new Author();
            firstAuthor.setFirstName("First Name 1");
            firstAuthor.setFirstName("Last Name 1");
            Author secondAuthor = new Author();
            secondAuthor.setFirstName("First Name 2");
            secondAuthor.setFirstName("Last Name 2");
            Author thirdAuthor = new Author();
            thirdAuthor.setFirstName("First Name 3");
            thirdAuthor.setFirstName("Last Name 3");
            Author fourthAuthor = new Author();
            fourthAuthor.setFirstName("First Name 4");
            fourthAuthor.setFirstName("Last Name 4");
            Book book = new Book();
            book.setName("Book 1");
            book.getAuthors().add(thirdAuthor);
            book.setEbookAvailable(false);
            book.setPublished(true);
            book.setStockAmount(40);
            book.setStringStockAmount("40");
            book.setPrice(23.45);
            books.add(book);
            book = new Book();
            book.setName("Book 2");
            book.getAuthors().add(secondAuthor);
            book.setEbookAvailable(true);
            book.setPublished(true);
            book.setStockAmount(0);
            book.setStringStockAmount("0");
            book.setPrice(123.57);
            books.add(book);
            book = new Book();
            book.setName("Book 3");
            book.getAuthors().add(firstAuthor);
            book.getAuthors().add(fourthAuthor);
            book.setEbookAvailable(false);
            book.setPublished(true);
            book.setStockAmount(0);
            book.setStringStockAmount("0");
            book.setPrice(94.81);
            books.add(book);
            book = new Book();
            book.setName("Book 4");
            book.getAuthors().add(firstAuthor);
            book.getAuthors().add(secondAuthor);
            book.setEbookAvailable(false);
            book.setPublished(true);
            book.setStockAmount(30);
            book.setStringStockAmount("30");
            book.setPrice(52.85);
            books.add(book);
            book = new Book();
            book.setName("Book 5");
            book.getAuthors().add(secondAuthor);
            book.getAuthors().add(thirdAuthor);
            book.getAuthors().add(fourthAuthor);
            book.setEbookAvailable(false);
            book.setPublished(true);
            book.setStockAmount(20);
            book.setStringStockAmount("20");
            book.setPrice(34.68);
            books.add(book);
            book = new Book();
            book.setName("Book 6");
            book.getAuthors().add(secondAuthor);
            book.setEbookAvailable(true);
            book.setPublished(false);
            book.setStockAmount(10);
            book.setStringStockAmount("10");
            book.setPrice(198.17);
            books.add(book);
        }
        return books;
    }
}

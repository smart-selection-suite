/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.suiteselection.domain.book;

/**
 *
 * @author imyousuf
 */
public class Author {

    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return new StringBuilder("").append(firstName == null ? "" : firstName).
            append(' ').append(lastName == null ? "" : lastName).toString();
    }

    @Override
    public String toString() {
        return getName();
    }
    
    
}

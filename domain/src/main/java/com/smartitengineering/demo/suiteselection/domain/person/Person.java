/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.demo.suiteselection.domain.person;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author imyousuf
 */
public class Person {
    private String name;
    private Set<Pet> pets;
    private Set<Person> children;
    private boolean dead;

    public Set<Person> getChildren() {
        if(children == null) {
            children = new HashSet<Person>();
        }
        return children;
    }

    public void setChildren(Set<Person> children) {
        this.children = children;
    }

    public String getName() {
        if(name == null) {
            name = "";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Pet> getPets() {
        if(pets == null) {
            pets = new HashSet<Pet>();
        }
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    public boolean isEmpty() {
        return isPetsEmpty() && isChildrenEmpty();
    }
    
    public boolean isPetsEmpty() {
        return (pets == null || pets.isEmpty());
    }

    public boolean isChildrenEmpty() {
        return children == null || children.isEmpty();
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }
}

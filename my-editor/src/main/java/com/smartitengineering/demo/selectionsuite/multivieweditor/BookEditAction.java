/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.selectionsuite.multivieweditor;

import com.smartitengineering.demo.selectionsuite.api.book.BookNode;
import java.awt.EventQueue;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.NodeAction;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.core.spi.multiview.MultiViewFactory;
import org.openide.windows.TopComponent;
import com.jgoodies.binding.PresentationModel;
import com.smartitengineering.demo.suiteselection.domain.book.Book;

/**
 *
 * @author modhu7
 */
public class BookEditAction extends NodeAction {
    public PresentationModel<Book> presentationModel;
    Book book;
    @Override
    protected void performAction(final Node[] nodes) {
        for (int i = 0; i < nodes.length; i++) {
            final int index = i;
            EventQueue.invokeLater(new Runnable() {

                public void run() {
                    BookNode node = (BookNode) nodes[index];
                    book = node.getBook();
                    presentationModel = new PresentationModel<Book>(book);
                    
                    final MultiViewDescription[] descriptions = {new BookView((BookNode) nodes[index], presentationModel), new BookEditor((BookNode) nodes[index], presentationModel)};
                    
                    TopComponent t = MultiViewFactory.createMultiView(descriptions, descriptions[1]);
                    t.setDisplayName("Multiview");
                    t.open();
                    t.requestActive();

                }
            });

        }
    }

    @Override
    protected boolean enable(Node[] nodes) {
        return true;
    }

    @Override
    public String getName() {
        return "Edit Action";
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

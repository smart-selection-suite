/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.selectionsuite.myeditor.books;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

/**
 * Action which shows BookView component.
 */
public class BookViewAction
    extends AbstractAction {

    public BookViewAction() {
        super(NbBundle.getMessage(BookViewAction.class, "CTL_BookViewAction"));
        putValue(SMALL_ICON, new ImageIcon(Utilities.loadImage(BookViewTopComponent.ICON_PATH, true)));
    }

    public void actionPerformed(ActionEvent evt) {
        TopComponent win = BookViewTopComponent.findInstance();
        win.open();
        win.requestActive();
    }
}

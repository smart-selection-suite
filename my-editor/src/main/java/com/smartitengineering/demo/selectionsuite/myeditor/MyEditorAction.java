/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.selectionsuite.myeditor;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

/**
 * Action which shows MyEditor component.
 */
public class MyEditorAction
    extends AbstractAction {

    public MyEditorAction() {
        super(NbBundle.getMessage(MyEditorAction.class, "CTL_MyEditorAction"));
        putValue(SMALL_ICON, new ImageIcon(Utilities.loadImage(MyEditorTopComponent.ICON_PATH, true)));
    }

    public void actionPerformed(ActionEvent evt) {
        TopComponent win = MyEditorTopComponent.findInstance();
        win.open();
        win.requestActive();        
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.selectionsuite.myeditor.books;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

/**
 * Action which shows SellableBooks component.
 */
public class SellableBooksAction
    extends AbstractAction {

    public SellableBooksAction() {
        super(NbBundle.getMessage(SellableBooksAction.class,
            "CTL_SellableBooksAction"));
        putValue(SMALL_ICON, new ImageIcon(Utilities.loadImage(SellableBooksTopComponent.ICON_PATH, true)));
    }

    public void actionPerformed(ActionEvent evt) {
        TopComponent win = SellableBooksTopComponent.findInstance();
        win.open();
        win.requestActive();
    }
}

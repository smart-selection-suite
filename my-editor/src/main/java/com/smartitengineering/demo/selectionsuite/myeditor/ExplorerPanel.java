/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.selectionsuite.myeditor;

import java.awt.LayoutManager;
import javax.swing.JPanel;
import org.openide.explorer.ExplorerManager;

/**
 *
 * @author imyousuf
 */
public class ExplorerPanel
    extends JPanel
    implements ExplorerManager.Provider {

    private ExplorerManager manager = null;

    public ExplorerPanel() {
        super();
    }

    public ExplorerPanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
    }

    public ExplorerPanel(LayoutManager layout) {
        super(layout);
    }

    public ExplorerPanel(LayoutManager layout,
                         boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
    }
    
    public ExplorerPanel(ExplorerManager manager) {
        super();
        this.manager = manager;
    }

    public ExplorerManager getExplorerManager() {
        if (manager == null) {
            manager = new ExplorerManager();
        }
        return manager;
    }

    public ExplorerManager getManager() {
        return manager;
    }

    public void setManager(ExplorerManager manager) {
        this.manager = manager;
    }
}

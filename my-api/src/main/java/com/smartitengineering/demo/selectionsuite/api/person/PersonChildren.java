/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.selectionsuite.api.person;

import com.smartitengineering.demo.suiteselection.domain.person.Person;
import com.smartitengineering.demo.suiteselection.domain.person.Pet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author imyousuf
 */
class PersonChildren
    extends Children.Keys<String> {

    protected static final String CHILDREN = "children";
    protected static final String PETS = "pets";
    protected static final String ROOT = "root";
    private Person person;
    private boolean showPetsEnabled;
    private boolean showChildrenEnabled;
    private List<Person> persons;

    public PersonChildren(final Person person,
                          final boolean showPetsEnabled,
                          final boolean showChildrenEnabled) {
        if (person == null) {
            throw new IllegalArgumentException();
        }
        this.person = person;
        this.showPetsEnabled = showPetsEnabled;
        this.showChildrenEnabled = showChildrenEnabled;
    }

    PersonChildren(final List<Person> persons,
                   final boolean showPetsEnabled,
                   final boolean showChildrenEnabled) {
        if (persons == null) {
            throw new IllegalArgumentException();
        }
        this.persons = persons;
        this.showPetsEnabled = showPetsEnabled;
        this.showChildrenEnabled = showChildrenEnabled;
    }

    @Override
    protected Node[] createNodes(String key) {
        if (key == null) {
            return new Node[0];
        }
        if (key.equals(CHILDREN)) {
            final Set<PersonNode> children;
            if (!person.isChildrenEmpty()) {
                final Set<Person> childrenSet = person.getChildren();
                children = new HashSet<PersonNode>();
                for (Person child : childrenSet) {
                    children.add(new PersonNode(child));
                }
            }
            else {
                children = null;
            }
            return person.isChildrenEmpty() ? new Node[0] : children.toArray(
                new Node[0]);
        }
        else if (key.equals(PETS)) {
            final Set<PetNode> pets;
            if (!person.isPetsEmpty()) {
                pets = new HashSet<PetNode>();
                final Set<Pet> petsSet = person.getPets();
                for (Pet pet : petsSet) {
                    pets.add(new PetNode(pet));
                }
            }
            else {
                pets = null;
            }
            return person.isPetsEmpty() ? new Node[0] : pets.toArray(
                new Node[0]);
        }
        else if (key.equals(ROOT)) {
            final Set<PersonNode> rootPersons = new HashSet<PersonNode>();
            if(!(persons == null || persons.isEmpty())) {
                for (Person root : persons) {
                    rootPersons.add(new PersonNode(root));
                }
            }
            return persons == null || persons.isEmpty() ? new Node[0] : 
                rootPersons.toArray(new Node[0]);
        }
        else {
            return new Node[0];
        }
    }

    @Override
    protected void addNotify() {
        super.addNotify();
        HashSet<String> keys = new HashSet<String>();
        if (persons != null && !persons.isEmpty()) {
            keys.add(ROOT);
        }
        else {
            if (showChildrenEnabled) {
                keys.add(CHILDREN);
            }
            if (showPetsEnabled) {
                keys.add(PETS);
            }
        }
        setKeys(keys.toArray(new String[0]));
    }
}

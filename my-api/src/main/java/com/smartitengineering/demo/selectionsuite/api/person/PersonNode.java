/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.selectionsuite.api.person;

import com.smartitengineering.demo.selectionsuite.api.actions.PersonNameAction;
import com.smartitengineering.demo.suiteselection.domain.DomainFactory;
import com.smartitengineering.demo.suiteselection.domain.person.Person;
import java.awt.Image;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.Utilities;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author imyousuf
 */
public class PersonNode
    extends AbstractNode {

    private Person person;
    public static PersonNameAction ACTION = new PersonNameAction();
            

    public PersonNode(Person person) {
        super(person == null || person.isChildrenEmpty() ? Children.LEAF : getPersonChildren(
            person));
        this.person = person;
        setDisplayName(this.person.getName());
        setIconBaseWithExtension("com/smartitengineering/demo/selectionsuite/myapi/person_root_icon.gif");
      }

    @Override
    public SystemAction[] getActions() {
        return new SystemAction[]{ACTION};
    }

    @Override
    public Action getPreferredAction() {
        return ACTION;
    }
    
    

    private static Children getPersonChildren(final Person person) {
        PersonChildren personChildren = new PersonChildren(person, false, true);
        return personChildren;
    }
    
    public static Children getRootPersonChildren() {
        PersonChildren personChildren = new PersonChildren(DomainFactory.getPersons(), false, true);
        return personChildren;
    }
    
    public static Children getPersonPetChildren(final Person person) {
        PersonChildren personChildren = new PersonChildren(person, true, false);
        return personChildren;
    }

    public Person getPerson() {
        return person;
    }

    @Override
    public Image getIcon(int arg0) {
        return Utilities.loadImage("com/smartitengineering/demo/selectionsuite/myapi/person_icon.gif");
    }

    @Override
    public String toString() {
        return person.getName();
    }
    
    
}

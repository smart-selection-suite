/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.selectionsuite.api.book;

import com.smartitengineering.demo.suiteselection.domain.book.Author;
import com.smartitengineering.demo.suiteselection.domain.book.Book;
import java.util.HashSet;
import java.util.Set;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author imyousuf
 */
public class BookChildren
    extends Children.Keys<String> {

    private Book book;

    public BookChildren(Book book) {
        if(book == null) {
            throw new IllegalArgumentException();
        }
        this.book = book;
    }

    @Override
    protected Node[] createNodes(String key) {
        Set<AuthorNode> authorSet = new HashSet<AuthorNode>();
        for(Author author : book.getAuthors()) {
            authorSet.add(new AuthorNode(author));
        }
        return authorSet.toArray(new Node[0]);
    }

    @Override
    protected void addNotify() {
        super.addNotify();
        setKeys(new String[]{"AUTHOR"});
    }
}

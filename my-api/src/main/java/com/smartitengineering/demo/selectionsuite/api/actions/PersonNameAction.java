/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.demo.selectionsuite.api.actions;

import com.smartitengineering.demo.selectionsuite.api.person.PersonNode;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.Lookup.Result;
import org.openide.util.Lookup.Template;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.actions.CallableSystemAction;

public final class PersonNameAction extends CallableSystemAction {

    public void performAction() {
        JOptionPane.showMessageDialog(null, "Hello from person");
    }

    public String getName() {
        return NbBundle.getMessage(PersonNameAction.class, "CTL_PersonNameAction");
    }

    @Override
    protected String iconResource() {
        return "com/smartitengineering/demo/selectionsuite/api/actions/root.gif";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    /*@Override
    public void actionPerformed(ActionEvent e) {
    JMenuItem item = (JMenuItem) e.getSource();
    PersonNode personNode = (PersonNode) item.getClientProperty("personChildren");
    JOptionPane.showMessageDialog(null, personNode.getPerson().getName());
    }*/
    
    public void actionPerformed(ActionEvent e){
        final Lookup actionsGlobalContext = Utilities.actionsGlobalContext();
        final Template<PersonNode> personNodeTemplate =
            new Lookup.Template<PersonNode>(PersonNode.class);
        Lookup.Result<PersonNode> result = actionsGlobalContext.lookup(personNodeTemplate);
        LookupListener personLookupListener = new LookupListener() {
            public void resultChanged(LookupEvent nodeEvent) {
                
            }           
        };
        result.addLookupListener(personLookupListener);
        result.allInstances();
        PersonNode personNode  = result.allInstances().iterator().next();
        JOptionPane.showMessageDialog(null, personNode.getPerson().getName());
        
    }
}

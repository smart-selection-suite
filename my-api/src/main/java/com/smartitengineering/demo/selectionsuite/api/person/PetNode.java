/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.demo.selectionsuite.api.person;

import com.smartitengineering.demo.suiteselection.domain.person.Pet;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 *
 * @author imyousuf
 */
public class PetNode extends AbstractNode{
    private Pet pet;
    
    public PetNode(Pet pet) {
        super(Children.LEAF);
        if(pet != null) {
            this.pet = pet;
        }
        else {
            pet = new Pet();
            pet.setName("");
        }
        setDisplayName(this.pet.getName());
        setIconBaseWithExtension("com/smartitengineering/demo/selectionsuite/myapi/pet_icon.gif");
    }

    public Pet getPet() {
        return pet;
    }
}

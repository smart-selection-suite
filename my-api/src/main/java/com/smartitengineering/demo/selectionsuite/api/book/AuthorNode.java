/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.demo.selectionsuite.api.book;

import com.smartitengineering.demo.suiteselection.domain.book.Author;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 *
 * @author imyousuf
 */
public class AuthorNode extends AbstractNode{

    public AuthorNode(Author author) {
        super(Children.LEAF);
        setDisplayName(author.getName());
        setIconBaseWithExtension("com/smartitengineering/demo/selectionsuite/myapi/person_icon.gif");
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.demo.selectionsuite.api.book;

import com.smartitengineering.demo.selectionsuite.api.actions.BookEditAction;
import com.smartitengineering.demo.suiteselection.domain.DomainFactory;
import com.smartitengineering.demo.suiteselection.domain.book.Book;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author imyousuf
 */
public class BookNode extends AbstractNode {
    protected BookNode bookNode;
    public static BookEditAction ACTION = new BookEditAction();
    private Book book;
    protected SystemAction[] passedActions;
    protected SystemAction[] localActions = new SystemAction[]{ACTION};

    private static Children getBookChildren(Book book) {
        return new BookChildren(book);
    }
    
    public static Children getRootBookChildren() {
        Children children = new Children.Array();
        List<Book> books = DomainFactory.getBooks();
        Node[] nodes = new Node[books.size()];
        int i = 0;
        for(Book book : books) {
            nodes[i++] = new BookNode(book, Children.LEAF);
        }
        children.add(nodes);
        return children;
    }
    
    public static Children getRootBookChildren(SystemAction...actions) {
        Children children = new Children.Array();
        List<Book> books = DomainFactory.getBooks();
        Node[] nodes = new Node[books.size()];
        int i = 0;
        for(Book book : books) {
            nodes[i++] = new BookNode(book, Children.LEAF, actions);
        }
        children.add(nodes);
        return children;
    }
    

    public BookNode(Book book) {
        this(book, getBookChildren(book));
    }
    
    private BookNode(Book book, Children children) {
        super(children);
        setDisplayName(book.getName());
        setIconBaseWithExtension("com/smartitengineering/demo/selectionsuite/myapi/book.gif");
        this.book = book;
        this.book.addPropertyChangeListener(new PropertyChangeListener() {

            public void propertyChange(PropertyChangeEvent evt) {
                if(evt.getPropertyName().equals(Book.PROPERTYNAME_NAME)){
                    setDisplayName(((Book)evt.getSource()).getName());
                }
            }
        });
    }

    private BookNode(Book book, Children children, SystemAction... actions) {
        this(book, children);        
        this.passedActions = actions;
        //System.out.println((this.passedActions.length+20));
    }
    
    public Book getBook() {
        return book;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BookNode)) {
            return false;
        }
        final BookNode other = (BookNode) obj;
        return this.book.getName().equals(other.getBook().getName());
    }
    
    public Node getCompleteBookNode() {
        if(bookNode == null) {
            bookNode = new BookNode(book);
        }
        return bookNode;
    }

    @Override
    public int hashCode() {
        return book.getName().hashCode();
    }
    
    public String getBookName() {
        return getBook().getName();
    }
    
    public double getPrice() {
        return getBook().getPrice();
    }

    public int getStockAmount() {
        return getBook().getStockAmount();
    }
    
    public String getStringStockAmount(){
        return getBook().getStringStockAmount();
    }

    public boolean isPublished() {
        return getBook().isPublished();
    }

    public boolean isEbookAvailable() {
        return getBook().isEbookAvailable();
    }

    @Override
    public SystemAction[] getActions() {
        SystemAction[] allActions = new SystemAction[this.passedActions.length + this.localActions.length];
        System.out.println((allActions.length+20));
        for(int i = 0; i < this.passedActions.length; i++){
            allActions[i] = this.passedActions[i];           
        }
        for (int i = 0; i < this.localActions.length; i++){
            allActions[this.passedActions.length+i] = this.localActions[i];
        }
        return allActions;
    }

    @Override
    public Action getPreferredAction() {
        return ACTION;
    }
    
}

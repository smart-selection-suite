/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.demo.selectionsuite.api.actions;

import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.NodeAction;





/**
 *
 * @author modhu7
 */
public class BookEditAction extends NodeAction{
    

    @Override
    protected void performAction(Node[] nodes) {
        
    }

    @Override
    protected boolean enable(Node[] nodes) {
        return true;
    }

    @Override
    public String getName() {
        return "Edit";
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}
